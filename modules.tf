
module "networking-dev" {
    source = "git::https://gitlab.com/TerraformExample/terraform-aws-networking.git?ref=v1.0.2"
    cidr = "10.1.0.0/16"
}

module "networking-prod" {
    source = "git::https://gitlab.com/TerraformExample/terraform-aws-networking.git?ref=v1.0.1"
    cidr = "10.2.0.0/16"
}

module "iam" {
    source = "git::https://gitlab.com/TerraformExample/terraform-aws-iam.git?ref=v1.0.0"
    superadmin_access_account = "588738232867"
}
