
variable "aws_profile" {
    description = "The AWS profile to use from ~/.aws/credentials"
    default = "default"
}
